from dearpygui import dearpygui as dpg
from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.injections import CodeInjectionUI
from trainerbase.gui.misc import SeparatorUI
from trainerbase.gui.objects import GameObjectUI
from trainerbase.gui.teleport import TeleportUI

from injections import infinite_items
from objects import bronze_talent_points, endurance, gold_talent_points, hp, money, silver_talent_points
from teleport import tp


@simple_trainerbase_menu("The Witcher: Enhanced Edition", 780, 320)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_components(
                GameObjectUI(hp, "HP", "Shift + F6", "F6", default_setter_input_value=5000.0),
                GameObjectUI(endurance, "Endurance", "Shift + F7", "F7", default_setter_input_value=500.0),
                GameObjectUI(money, "Money", default_setter_input_value=1_000_000),
                SeparatorUI(),
                GameObjectUI(bronze_talent_points, "Bronze", default_setter_input_value=100),
                GameObjectUI(silver_talent_points, "Silver", default_setter_input_value=100),
                GameObjectUI(gold_talent_points, "Gold", default_setter_input_value=100),
                SeparatorUI(),
                CodeInjectionUI(infinite_items, "Infinite Items", "F8"),
            )

        with dpg.tab(label="Teleport"):
            add_components(TeleportUI(tp))
