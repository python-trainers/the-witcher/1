from trainerbase.codeinjection import AllocatingCodeInjection
from trainerbase.process import pm

from memory import money_address, player_status_address


update_money_address = AllocatingCodeInjection(
    pm.base_address + 0x4EED5B,
    f"""
        mov [{money_address.base_address}], esi

        mov ecx, [esi + 0xA40]
    """,
    original_code_length=6,
)


update_player_status_address = AllocatingCodeInjection(
    pm.base_address + 0x37A59D,
    f"""
        cmp dword [ecx + 0x7c], 'Wied'  ; if entity name starts with 'Wied'
        jne skip

        mov [{player_status_address.base_address}], ecx

        skip:
        fld dword [ecx + 0x54]
        fadd dword [ecx + 0x48]
    """,
    original_code_length=6,
)

infinite_items = AllocatingCodeInjection(
    pm.base_address + 0x4B1B61,
    """
        inc dword [esi + 0x424]

        mov eax, [esi + 0x424]
    """,
    original_code_length=6,
)
