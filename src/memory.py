from trainerbase.memory import Address, allocate


player_status_address = Address(allocate())
money_address = Address(allocate(), [0xA40])
