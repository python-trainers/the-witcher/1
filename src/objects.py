from trainerbase.gameobject import GameFloat, GameUnsignedInt

from memory import money_address, player_status_address


coords_address = player_status_address + [0x18]
player_x = GameFloat(coords_address)
player_y = GameFloat(coords_address + 0x4)
player_z = GameFloat(coords_address + 0x8)

hp = GameFloat(player_status_address + [0x48])
endurance = GameFloat(player_status_address + [0x1AC])

talent_points_address = player_status_address + [0x294]
bronze_talent_points = GameUnsignedInt(talent_points_address)
silver_talent_points = GameUnsignedInt(talent_points_address + 0x4)
gold_talent_points = GameUnsignedInt(talent_points_address + 0x8)

money = GameUnsignedInt(money_address)
