from trainerbase.main import run

from gui import run_menu
from injections import update_money_address, update_player_status_address


def on_initialized():
    update_player_status_address.inject()
    update_money_address.inject()


if __name__ == "__main__":
    run(run_menu, on_initialized)
